Pod::Spec.new do |s|
  s.name            = "KeyCloakSDK"
  s.version         = "1.0.0"
  s.summary         = "KeyCloakSDK"
  s.description     = "KeyCloakSDK for iOS"
  s.homepage        = "https://gitlab.com/mobilevas/vasmobileframework-public"
  s.license         = { :type => 'MIT', :file => 'LICENSE' }
  s.author          = { "Ruchupong Saengan" => "Ruchupong_sae@truecorp.co.th" }
  s.source          = { :git => "git@gitlab.com:mobilevas/vasmobileframework-ios.git", :tag => "keycloaksdk-1.0.0_rc1" }
  s.platform        = :ios, "11.0"
  s.module_name     = s.name

  s.vendored_frameworks    = "*.xcframework"

  s.dependency 'Alamofire', '5.4'

end