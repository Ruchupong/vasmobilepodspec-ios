Pod::Spec.new do |s|
    s.name            = "VasSmartCardReader"
    s.version         = "1.2.0"
    s.summary         = "Library SmartCardReader (FEITIAN_MOBILE_READERS)"
    s.description     = "Library SmartCardReader (FEITIAN_MOBILE_READERS) for iOS"
    s.homepage        = "https://gitlab.com/mobilevas/vasmobileframework-ios"
    s.license         = { :type => 'MIT', :file => 'LICENSE' }
    s.author          = { "Ruchupong Saengan" => "Ruchupong_sae@truecorp.co.th" }
    s.source          = { :git => "git@gitlab.com:mobilevas/vasmobileframework-ios.git", :tag => "vassmartcardreader-1.2.0" }
    s.platform        = :ios, "11.0"
    s.module_name     = s.name

    s.vendored_frameworks    = "*/*.xcframework"

  end
  
  